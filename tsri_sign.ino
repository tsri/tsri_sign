#include "Adafruit_WS2801.h"
#include "SPI.h" // Comment out this line if using Trinket or Gemma
#ifdef __AVR_ATtiny85__
    #include <avr/power.h>
#endif

/*****************************************************************************
Example sketch for driving Adafruit WS2801 pixels!


  Designed specifically to work with the Adafruit RGB Pixels!
  12mm Bullet shape ----> https://www.adafruit.com/products/322
  12mm Flat shape   ----> https://www.adafruit.com/products/738
  36mm Square shape ----> https://www.adafruit.com/products/683

  These pixels use SPI to transmit the color data, and have built in
  high speed PWM drivers for 24 bit color per pixel
  2 pins are required to interface

  Adafruit invests time and resources providing this open source code,
  please support Adafruit and open-source hardware by purchasing
  products from Adafruit!

  Written by Limor Fried/Ladyada for Adafruit Industries.
  BSD license, all text above must be included in any redistribution

*****************************************************************************/

// Choose which 2 pins you will use for output.
// Can be any valid output pins.
// The colors of the wires may be totally different so
// BE SURE TO CHECK YOUR PIXELS TO SEE WHICH WIRES TO USE!
uint8_t dataPin  = 2;    // Yellow wire on Adafruit Pixels
uint8_t clockPin = 4;    // Green wire on Adafruit Pixels
uint8_t interruptPin = 3; // Switch

// Don't forget to connect the ground wire to Arduino ground,
// and the +5V wire to a +5V supply

// Set the first variable to the NUMBER of pixels. 25 = 25 pixels in a row
Adafruit_WS2801 strip = Adafruit_WS2801(50, dataPin, clockPin);

// Optional: leave off pin numbers to use hardware SPI
// (pinout is then specific to each board and can't be changed)
//Adafruit_WS2801 strip = Adafruit_WS2801(25);

// For 36mm LED pixels: these pixels internally represent color in a
// different format.  Either of the above constructors can accept an
// optional extra parameter: WS2801_RGB is 'conventional' RGB order
// WS2801_GRB is the GRB order required by the 36mm pixels.  Other
// than this parameter, your code does not need to do anything different;
// the library will handle the format change.  Examples:
//Adafruit_WS2801 strip = Adafruit_WS2801(25, dataPin, clockPin, WS2801_GRB);
//Adafruit_WS2801 strip = Adafruit_WS2801(25, WS2801_GRB);


// Create a 24 bit color value from R,G,B
uint32_t Color(byte r, byte g, byte b)
{
    uint32_t c;
    c = r;
    c <<= 8;
    c |= g;
    c <<= 8;
    c |= b;
    return c;
}


//TSÜRI Letters
int LETTERS[5][2] = {
  {0,7},
  {7,12},
  {19,11},
  {30,14},
  {44,6}
};

int MODE = 0;
bool MODE_CHANGE = true;

uint32_t TSRI_BLUE = Color(54, 173, 223);
uint32_t TSRI_PINK = Color(236, 0, 140);

uint32_t COLORS[5] = {
  Color(255, 25, 25),
  TSRI_BLUE,
  TSRI_PINK,
  Color(255, 224, 25),
  Color(40, 237, 26)
};

uint32_t COLORS_FRUIT[5] = {
  Color(255, 231, 76),
  Color(255, 89, 100),
  Color(255, 255, 255),
  Color(107, 241, 120),
  Color(53, 167, 255)
};

void setup() {
  #if defined(__AVR_ATtiny85__) && (F_CPU == 16000000L)
    clock_prescale_set(clock_div_1); // Enable 16 MHz on Trinket
  #endif

  Serial.begin(9600);
  pinMode(interruptPin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(interruptPin), changeMode, FALLING);
  
  strip.begin();
  // Update LED contents, to start they are all 'off'
  strip.show();
}

void changeMode() {
  Serial.println("CHANGE MODE");
  Serial.println(MODE);
  if (MODE_CHANGE != true) {
    Serial.println("INSIDE");
    MODE_CHANGE = true;
    if(MODE < 6) {
      MODE = MODE + 1;
    } else {
      MODE = 0;
    }
  }
}

void rainbow(uint8_t wait) {
    int i, j;

    for (j=0; j < 256; j++) {     // 3 cycles of all 256 colors in the wheel
        for (i=0; i < strip.numPixels(); i++) {
            strip.setPixelColor(i, Wheel( (i + j) % 255));
        }
        strip.show();   // write all the pixels out
        delay(wait);
    }
}

// Slightly different, this one makes the rainbow wheel equally distributed
// along the chain
void rainbowCycle(uint8_t wait) {
    int i, j;

    //for (j=0; j < 256 * 5; j++) {     // 5 cycles of all 25 colors in the wheel
    for (j=0; j < 256 * 5; j++) {     // 5 cycles of all 25 colors in the wheel
        for (i=0; i < strip.numPixels(); i++) {
            // tricky math! we use each pixel as a fraction of the full 96-color wheel
            // (thats the i / strip.numPixels() part)
            // Then add in j which makes the colors go around per pixel
            // the % 96 is to make the wheel cycle around
            strip.setPixelColor(i, Wheel( ((i * 256 / strip.numPixels()) + j) % 256) );
        }
        strip.show();   // write all the pixels out
        if (MODE_CHANGE == true) {
        break;
      } else {
        delay(wait);
      }
    }
}

// fill the dots one after the other with said color
// good for testing purposes
void colorWipe(uint32_t c, uint8_t wait) {
    int i;

    for (i=0; i < strip.numPixels(); i++) {
        strip.setPixelColor(i, c);
        strip.show();
        delay(wait);
    }
}

/* Helper functions */


//Input a value 0 to 255 to get a color value.
//The colours are a transition r - g -b - back to r
uint32_t Wheel(byte WheelPos)
{
    if (WheelPos < 85) {
        return Color(WheelPos * 3, 255 - WheelPos * 3, 0);
    } else if (WheelPos < 170) {
        WheelPos -= 85;
        return Color(255 - WheelPos * 3, 0, WheelPos * 3);
    } else {
        WheelPos -= 170;
        return Color(0, WheelPos * 3, 255 - WheelPos * 3);
    }
}

void letterRainbow(uint8_t wait) {
    int i, j, x;

    for (j=0; j < 256 * 5; j++) {     // 5 cycles of all 256 colors in the wheel
        for (i=0; i < 5; i++) {     // 5 letters
          uint32_t color = Wheel( ((i * 256 / 5) + j) % 256);
          for (x=0; x < LETTERS[j][1]; x++) {
              strip.setPixelColor(LETTERS[i][0] + x, color);
          }
        }
        strip.show();   // write all the pixels out
        if (MODE_CHANGE == true) {
          break;
        } else {
          Serial.println("Waiting letterRainbow");
          delay(wait);
        }
    }
    
    
}


void letterJump(uint32_t colorPalette[], uint32_t wait) {
  int i, j, x;
  for (x=0; x < 5; x++){
    for (j=0; j < 5; j++) {
      Serial.print((j+x) % 5);
      uint32_t color = colorPalette[(j+x) % 5];
      for (i=0; i < LETTERS[j][1]; i++) {
        strip.setPixelColor(LETTERS[j][0] + i, color);
      }
    }
    strip.show();
    if (MODE_CHANGE == true) {
      break;
    } else {
      Serial.println("Waiting letterJump");
      delay(wait);
    }
  } 
}

void strobo(uint32_t color, uint32_t wait) {
  while(MODE_CHANGE == false) {
    colorWipe(color, 1);
    delay(wait);
    colorWipe(Color(0,0,0), 1);
  }
}


void loop() {
    // Some example procedures showing how to display to the pixels
    Serial.println("Start");
    switch(MODE) {
      case 0:
        Serial.println("MODE 0");
        colorWipe(Color(54, 173, 223), 10);
        break;
      case 1:
        Serial.println("MODE 1");
        colorWipe(Color(236, 0, 140), 10);
        break;
      case 2:
        Serial.println("MODE 2");
        letterJump(COLORS, 2000);
        break;
      case 3:
        Serial.println("MODE 3");
        rainbowCycle(20);
        // letterRainbow(20);
        break;
      case 4:
        Serial.println("MODE 4");
        letterJump(COLORS_FRUIT, 1000);
        break;
      case 5:
        Serial.println("MODE 5");
        strobo(TSRI_BLUE, 10);
        break;
      case 6:
        Serial.println("MODE 6");
        strobo(TSRI_PINK, 10);
        break;
    }
    MODE_CHANGE = false;
    delay(1000);
    Serial.println("Done");
}
